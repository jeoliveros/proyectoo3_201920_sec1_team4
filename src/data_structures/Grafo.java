package data_structures;

import java.util.Iterator;

public class Grafo <K,V,C>
{
	private int cantNodos;
	private int cantArcos;
	private LinearProbingHashST<K, V> nodos;
	private LinearProbingHashST<K, LinearProbingHashST<K,C>> arcos;
	
	public Grafo()
	{
		cantNodos = 0;
		cantArcos = 0;
		nodos = new LinearProbingHashST<K, V>();
		arcos = new LinearProbingHashST<K, LinearProbingHashST<K,C>>();
	}
	
	public Grafo(int cantVertices)
	{
		cantNodos = 0;
		cantArcos = 0;
		nodos = new LinearProbingHashST<K, V>(cantVertices);
		arcos = new LinearProbingHashST<K, LinearProbingHashST<K,C>>(cantVertices);
	}
	
	public Grafo(int cantVertices, int cantArcos)
	{
		cantNodos = 0;
		cantArcos = 0;
		nodos = new LinearProbingHashST<K, V>(cantVertices);
		arcos = new LinearProbingHashST<K, LinearProbingHashST<K,C>>(cantArcos);
	}
	
	public int getCantNodos() {
		return cantNodos;
	}
	
	public int getCantArcos() {
		return cantArcos;
	}
	
	public boolean addVertice(K llave, V valor)
	{
		if(!nodos.contains(llave))
		{
			nodos.put(llave, valor);
			cantNodos++;
			return true;
		}
		return false;
	}
	
	public boolean setVertice(K llave, V valor)
	{
		if(nodos.contains(llave))
		{
			nodos.put(llave, valor);
			return true;
		}
		return false;
	}
	
	public void addArco(K origen, K destino, C costo)
	{
		if(!(nodos.contains(origen) && nodos.contains(destino))) return;
		LinearProbingHashST<K,C> hashDestinos = arcos.get(origen);
		if(hashDestinos == null)
		{
			hashDestinos = new LinearProbingHashST<K, C>();
			arcos.put(origen, hashDestinos);
		}
		hashDestinos.put(destino,costo);
		cantArcos++;
	}
	
	public V getVertice(K llave)
	{
		return nodos.get(llave);
	}
	
	public C getCosto(K origen, K destino)
	{
		return getCostoReverso(origen, destino, false);
	}
	
	private C getCostoReverso(K origen, K destino, boolean cambio)
	{
		LinearProbingHashST<K,C> hash = arcos.get(origen);

		if(hash == null)
			if(cambio)
				return null;
			else
				return getCostoReverso(destino, origen, true);
		
		C costo = hash.get(destino);
		if(costo == null) 
			if(cambio)
				return null;
			else 
				return getCostoReverso(destino, origen, true);
		
		return costo;
	}
	
	public Iterable<K> getAdyacentes(K llave)
	{
		return arcos.get(llave).keys();
	}
	
	public Iterable<K> getKeys()
	{
		return nodos.keys();
	}
	
	public Iterable<V> getValues()
	{
		return nodos.values();
	}
	
	public Iterable<C> getCostos()
	{
		Queue<C> queue = new Queue<C>();
		arcos.values().forEach( (th) -> { th.values().forEach( (costo) -> { queue.enqueue(costo); } ); } );
		return queue;
	}
	
	
	

	
	
	
	
}

