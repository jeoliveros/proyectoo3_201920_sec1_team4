package view;

import logic.MVCModelo;


public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("0. Crear Grafo con los datos de mallas viales de Bogota ");
			System.out.println("1. Crear un archivo JSON para guardarlo.");
			System.out.println("2. cargar el nuevo grafo JSON generado.");
			System.out.println("3. Encontrar el Id del Vértice de la malla vial más cercano por distancia Haversine.");
			System.out.println("4. Encontrar el camino de costo mínimo (menor tiempo promedio según Uber en la ruta)");
			System.out.println("5. Determinar los n vértices con menor velocidad promedio en la ciudad de Bogotá.");
			System.out.println("6. Calcular un árbol de expansión mínima (MST) con criterio distancia, utilizando el algoritmo de Prim");
			System.out.println("7. Encontrar el camino de menor costo (menor distancia Haversine) para un viaje entre dos localizaciones geográficas");
			System.out.println("8. A partir de las coordenadas de una localización geográfica de la ciudad (lat, lon) de origen, indique cuáles vértices son alcanzables para un tiempo T (en segundos) dado por el usuario. ");
			System.out.println("9. Calcular un árbol de expansión mínima (MST) con criterio distancia, utilizando el algoritmo de Kruskal");
			System.out.println("10.Construir un nuevo grafo simplificado No dirigido de las zonas Uber, donde cada zona (MOVEMENT_ID) es representada con un único vértice y los enlaces entre ellas representan su vecindad dentro de la malla vial.  ");
			System.out.println("11.Calcular el camino de costo mínimo (algoritmo de Dijkstra) basado en el tiempo promedio entre una zona de origen y una zona de destino sobre el grafo de zonas.");
			System.out.println("12. calcular los caminos de menor longitud (cantidad de arcos) a todas sus zonas alcanzables.");
			System.out.println("13. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			System.out.println(modelo);
		}
}
