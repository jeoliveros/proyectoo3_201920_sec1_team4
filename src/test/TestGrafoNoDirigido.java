package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.sun.tools.sjavac.comp.dependencies.NewDependencyCollector;

import data_structures.Grafo;
import logic.Costo;
import logic.Interseccion;



public class TestGrafoNoDirigido  
{

	private Grafo<Integer, String, Costo> grafo;

	@Before
	public void setUpEscenario1()
	{
		grafo = new Grafo<Integer, String, Costo>(10);

		//Se crean los v�rtices

		grafo.addVertice(1, "Uno");

		grafo.addVertice(2, "Dos");

		grafo.addVertice(3, "Tres");

		grafo.addVertice(4, "Cuatro");

		grafo.addVertice(5, "Cinco");

		grafo.addVertice(6, "Seis");

		grafo.addVertice(7, "Siete");

		grafo.addVertice(8, "Ocho");

		//Se crean los arcos, quedando 4 componentes conectadas

		for (int i = 0; i < 4; i++) 
		{
			int tempa = i;
			int tempb = tempa++;
			
			Interseccion I1a = new Interseccion(tempa, 4.65, 7.54, 643);
			Interseccion I1b = new Interseccion(tempb, 4.65, 7.54, 643);
			
			
			Costo c1 = new Costo(I1a, I1b, 128, 232);
			
			grafo.addArco(tempa, tempb, c1);
		}
	}

	@Test
	public void testAddEdge()
	{
		
		int tempa = 2;
		int tempb = tempa++;
		
		Interseccion I1a = new Interseccion(tempa, 4.65, 7.54, 643);
		Interseccion I1b = new Interseccion(tempb, 4.65, 7.54, 643);
		Costo c1 = new Costo(I1a, I1b, 128, 232);
		
		grafo.addArco(tempa, tempb, c1);

		assertTrue("El arco deber�a existir", grafo.getCosto(2, 3);
				


	}

	@Test
	public void testGetInfoVertex()
	{
		assertEquals("La informaci�n del v�rtice no es la esperada", "Uno", grafo.getInfoVertex(1));

		assertEquals("La informaci�n del v�rtice no es la esperada", "Dos", grafo.getInfoVertex(2));
	}

	@Test
	public void testSetInfoVertex()
	{
		grafo.setInfoVertex(1, "Uno");

		assertEquals("La informaci�n del v�rtice no es la esperada", "Uno", grafo.getInfoVertex(1));

		grafo.setInfoVertex(2, "Dos");

		assertEquals("La informaci�n del v�rtice no es la esperada", "Deux", grafo.getInfoVertex(2));
	}

	@Test
	public void testGetCostArc()
	{
		assertEquals("El costo del arco no es el esperado", (Double) 1.0, (Double) grafo.getCostArc(1, 2));

		assertEquals("El costo del arco no es el esperado", (Double) 1.0, (Double) grafo.getCostArc(idVertexIni, idVertexFin));
	}
sdipwdcdsadasacaca
	@Test
	public void testSetCostArc()
	{
		grafo.setCostArc(1, 2, 5.2);
		assertEquals("El costo del arco no es el esperado", (Double) 5.2, (Double) grafo.getCostArc(1, 2));

		grafo.setCostArc(4, 3, 8.6);
		assertEquals("El costo del arco no es el esperado", (Double) 8.6, (Double) grafo.getCostArc(3, 4));
	}

	@Test
	public void testAddVertex()
	{
		grafo.addVertice(9, "Nueve");

		grafo.addVertice(10, "Diez");

		assertTrue("El v�rtice deber�a existir", grafo.existeVertice(9));
		assertTrue("El v�rtice deber�a existir", grafo.existeVertice(10));
	}

	@Test
	public void testAdj()
	{
		grafo.AgregarArco(2, 3, 1, -1, -1);
		grafo.AgregarArco(4, 2, 1, -1, -1);

		Iterator<Integer> iteradorIDs = grafo.adj(2).iterator();

		assertNotNull("Deber�a existir este v�rtice adyacente", iteradorIDs.next());
		assertNotNull("Deber�a existir este v�rtice adyacente", iteradorIDs.next());
		assertNotNull("Deber�a existir este v�rtice adyacente", iteradorIDs.next());

		try
		{
			iteradorIDs.next();
			fail("Deber�a arrojar una excepci�n");
		}
		catch (Exception e) {

		}
	}

	@Test
	public void testUncheck()
	{
		grafo.getVertex(1).check();
		grafo.getVertex(2).check();

		assertTrue("El v�rtice deber�a estar marcado", grafo.getVertex(1).isChecked());
		assertTrue("El v�rtice deber�a estar marcado", grafo.getVertex(2).isChecked());

		grafo.uncheck();

		assertFalse("El v�rtice no deber�a estar marcado", grafo.getVertex(1).isChecked());
		assertFalse("El v�rtice no deber�a estar marcado", grafo.getVertex(2).isChecked());
	}

	@Test
	public void testDFS()
	{
		grafo.AgregarArco(2, 3, 1, -1, -1);
		grafo.AgregarArco(4, 2, 1, -1, -1);

		grafo.dfs(2, 2);

		assertTrue("El v�rtice deber�a estar marcado", graph.getVertex(1).isChecked());
		assertTrue("El v�rtice deber�a estar marcado", graph.getVertex(2).isChecked());
		assertTrue("El v�rtice deber�a estar marcado", graph.getVertex(3).isChecked());
		assertTrue("El v�rtice deber�a estar marcado", graph.getVertex(4).isChecked());

		assertEquals("La componente del v�rtice no es la esperada", 2, graph.getVertex(1).componente);
		assertEquals("La componente del v�rtice no es la esperada", 2, graph.getVertex(2).componente);
		assertEquals("La componente del v�rtice no es la esperada", 2, graph.getVertex(3).componente);
		assertEquals("La componente del v�rtice no es la esperada", 2, graph.getVertex(4).componente);
	}

	@Test
	public void testCC()
	{
		int cantidadComponentes = grafo.cc();

		assertEquals("La cantidad de componentes no es la esperada", 4, cantidadComponentes);

		grafo.AgregarArco(2, 3, 1, -1, -1);

		cantidadComponentes = grafo.cc();

		assertEquals("La cantidad de componentes no es la esperada", 3, cantidadComponentes);
	}

	@Test
	public void testGetCC()
	{
		graph.addEdge(2, 3, 1, -1, -1);

		graph.cc();

		try {
			Iterator<Integer> iterator = graph.getCC(1).iterator();

			assertNotNull("Deber�a llegar a este v�rtice", iterator.next());
			assertNotNull("Deber�a llegar a este v�rtice", iterator.next());
			assertNotNull("Deber�a llegar a este v�rtice", iterator.next());

			try
			{
				iterator.next();
				fail("Deber�a arrojar una excepci�n");
			}
			catch (Exception e) {

			}

		} catch (Exception e) {
			fail("No deber�a arrojar la excepci�n");
		}
	}
}
