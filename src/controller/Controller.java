package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import data_structures.Grafo;
import logic.Interseccion;
import logic.MVCModelo;
import view.MVCView;

public class Controller 
{
	//Atributo
	//public GrafoNoDirigido grafoNoDirigido;

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}



	public void run() throws Exception 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){

			case 0:
				// R_0

				System.out.println("--------- \nCreando Grafo \n-------------------- ");
				
				long time = System.currentTimeMillis();

				modelo.CargarArchivos();	
				//modelo.MostrarMapa();
				
				time = System.currentTimeMillis() - time;
				view.printMessage("---------------------------");
				view.printMessage("Tiempos transcurrido: "+time+" ms");
				

				break;

			case 1:
				// R_2
				
				System.out.println("--------- \nSe esta creando el archivo Json \n-------------------- ");
				
				long time1 = System.currentTimeMillis();
		
				modelo.GenerarGrafoJSON();
				
				time1 = System.currentTimeMillis() - time1;
				view.printMessage("---------------------------");
				view.printMessage("Tiempos transcurrido: "+time1+" ms");
				
			
				break;

			case 2:


				System.out.println("--------- \nSe esta cargando el archivo Json"+"\n---------------");
				
				
				long time2 = System.currentTimeMillis();
				modelo.CargarGrafoJSON();
				
				
				time2 = System.currentTimeMillis() - time2;
				view.printMessage("---------------------------");
				view.printMessage("Tiempos transcurrido: "+time2+" ms");
				
				
//				modelo.crearArchivoJSON();
//				System.out.println("Se cargo el archivo se carga");
//
//				System.out.println("Se creo correctamente el archivo Json y se emepzo a cargar"+"\n---------------");
//				modelo.cargarArchivoJSON();		
//
//				view.printMessage("El grafo se cargo correctamente \n---------------");
//				view.printMessage("La cantidad de vertices en el grafo es:"+ modelo.darNumVerticesGrafoJson()+"\n---------------" );
//				view.printMessage("La cantidad de arcos en el grafo es:"+ modelo.darNumArcosGrafoJson()+"\n---------------");
				
				break;

			case 3:
				

				System.out.println("--------- \nSe esta buscando el vertice mas cercano"+"\n---------------");

				
				
				view.printMessage("Ingrese la latitud de la interseccion");
				int platitud3 = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese La Longitud de la interseccion ");
				int plongitud3 = Integer.parseInt(lector.next());
				
		
				int id =modelo.DarIDMasCercano(platitud3, platitud3);
				
				view.printMessage("El ID del vertice mas ecrcano es:"+ id+"\n---------------");
				
				break;

			case 4:
				
				System.out.println("--------- \n Digite la laitud y longitud del vertice de origen y del de destino"+"\n---------------");

				view.printMessage("Ingrese La latitud de la interseccion de origen");
				int platitudOrigen = Integer.parseInt(lector.next());       
				view.printMessage("Ingrese  La Longitud de la interseccion de origen ");
				int plongitudOreigen = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese  La latitud de la interseccion de Destino ");
				int platitudDestino = Integer.parseInt(lector.next());
				view.printMessage("Ingrese  La Longitud de la interseccion de Destino  ");
				int plongitudDestino = Integer.parseInt(lector.next());
				
				
				
				int numvertices=0;
				double costominimo4=0;
				double distaciaMinima4=0;
				
				view.printMessage("La cantidad total de vértices es"+numvertices+"\n---------------" );
				
				for (int i = 0; i < numvertices; i++) 
				{
					int iDvertice4 =0;
					double lat4 =0;
					double long4 =0;
					
					view.printMessage("El vertice con ID:"+iDvertice4+" Su latitud es: "+lat4 +" Su longitud es: "+long4+"\n---------------" );
				}

				
				view.printMessage("El costo minimo en segundo es: "+costominimo4+"\n---------------" );
				view.printMessage("La distancia estimada en km es: "+distaciaMinima4+"\n---------------" );
				

				break;

			case 5: 

				System.out.println("--------- \n Se esta Graficando con ayuda de Google Maps la parte del grafo resultante"+"\n---------------");
				

				break;	

			case 6: 
				System.out.println("--------- \n Calculando MTS con el algoritmo de Prim !! \n---------"); 
			
				long startTime6 = System.currentTimeMillis();
				//realizar metodo
				
				//nose donde edita 
				long endTime6 = System.currentTimeMillis(); 
				
				long duration6 = endTime6 - startTime6; 
				
				int totalverticesComponente6=0;
				
				
				view.printMessage("El tiempo que le tomo al algoritmo realizar la busqeuda en milisegundos es:"+ duration6+"\n---------------");
			
				
				
				view.printMessage("El total de vértices en el componente:"+ totalverticesComponente6+"\n---------------");
				
				
				
				int verticesIdentificadores6 =0;	
				int	idVerticeinial6=0;
				int	idVerticefinal6=0;
				double  costoTotal = 0;
				
				
				view.printMessage("Los vertices identificadores son:"+ verticesIdentificadores6+"\n---------------");
				
				
				view.printMessage("El arco numero:"+0+ "tiene como vertice inicial"+idVerticeinial6+" y tiene como Vertice Final:" +idVerticefinal6 + "\n---------------");
				
				view.printMessage("El costo total en km es de:"+ costoTotal+"\n---------------");
				
				
				break;	

			case 7:
				
				System.out.println("--------- \n Digite la laitud y longitud del vertice de origen y del de destino"+"\n---------------");

				view.printMessage("Ingrese La latitud de la interseccion de origen");
				int platitudOrigen7 = Integer.parseInt(lector.next());       
				view.printMessage("Ingrese  La Longitud de la interseccion de origen ");
				int plongitudOreigen7 = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese  La latitud de la interseccion de Destino ");
				int platitudDestino7 = Integer.parseInt(lector.next());
				view.printMessage("Ingrese  La Longitud de la interseccion de Destino  ");
				int plongitudDestino7 = Integer.parseInt(lector.next());
				
				
				
				int numvertices7=0;
				double costominimo7=0;
				double distaciaMinima7=0;
				
				view.printMessage("La cantidad total de vértices es"+numvertices7+"\n---------------" );
				
				for (int i = 0; i < numvertices7; i++) 
				{
					int iDvertice7 =0;
					double lat7 =0;
					double long7 =0;
					
					view.printMessage("El vertice con ID:"+iDvertice7+" Su latitud es: "+lat7 +" Su longitud es: "+long7+"\n---------------" );
				}

				
				view.printMessage("El costo minimo en segundo es: "+costominimo7+"\n---------------" );
				view.printMessage("La distancia estimada en km es: "+distaciaMinima7+"\n---------------" );
				
				
				break;
				
				
			case 8:
				
				
				break;
				
			case 9:
				
				System.out.println("--------- \n Calculando MTS con el algoritmo de Kruskal !! \n---------"); 
				
				 
				
				long startTime9 = System.currentTimeMillis();
				//realizar metodo
				long endTime9 = System.currentTimeMillis(); 
				
				long duration9 = endTime9 - startTime9; 
				
				int totalverticesComponente9=0;
				
				
				view.printMessage("El tiempo que le tomo al algoritmo realizar la busqeuda en milisegundos es:"+ duration9+"\n---------------");
			
				
				
				view.printMessage("El total de vértices en el componente:"+ totalverticesComponente9+"\n---------------");
				
				
				
				int verticesIdentificadores9 =0;	
				int	idVerticeinial9=0;
				int	idVerticefinal9=0;
				double  costoTotal9 = 0;
				
				
				view.printMessage("Los vertices identificadores son:"+ verticesIdentificadores9+"\n---------------");
				
				
				view.printMessage("El arco numero:"+0+ "tiene como vertice inicial"+idVerticeinial9+" y tiene como Vertice Final:" +idVerticefinal9 + "\n---------------");
				
				view.printMessage("El costo total en km es de:"+ costoTotal9+"\n---------------");
				
				
				
				break;
				
			case 10:
				break;
				
			case 11:
				break;
				
				
			case 12:
				
				long startTime12 = System.currentTimeMillis();
				//realizar metodo
				long endTime12 = System.currentTimeMillis(); 
				
				long duration12 = endTime12 - startTime12; 
				
				
				
				int numArcos=0;
				for (int i = 0; i < 80; i++) 
				{
					
					int idvertice=0;
					int idezona= 0;
					
					view.printMessage("Secuencia de vertice:"+ idvertice +" con zona:"+idezona+"\n---------------");		
				}
				view.printMessage("El numero total de arcos es:"+ numArcos +"\n---------------");
				
				break;
				
				
				
			case 13:
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
