package logic;

public class Costo
{
	private Interseccion idInterseccionOrigen;
	private Interseccion idInterseccionDestino;
	
	private double costDistanciaHarversine;
	private double costTiempoDeViaje;
	private double costVelocidad;
	private int cantidadDistancia;
	private int cantidadTiempo;
	

	public Costo(Interseccion idInterseccionOrigen, Interseccion idInterseccionDestino) {
		this.idInterseccionOrigen = idInterseccionOrigen;
		this.idInterseccionDestino = idInterseccionDestino;
		this.costDistanciaHarversine = 0;
		this.costTiempoDeViaje = 0;
		this.costVelocidad = 0;
		this.cantidadTiempo = 0;
		this.cantidadDistancia = 0;
	}
	
	public Costo(Interseccion idInterseccionOrigen, Interseccion idInterseccionDestino, double costDistanciaHarversine, double costTiempoDeViaje) {
		this.idInterseccionOrigen = idInterseccionOrigen;
		this.idInterseccionDestino = idInterseccionDestino;
		this.costDistanciaHarversine = costDistanciaHarversine;
		this.costTiempoDeViaje = costTiempoDeViaje;
		this.costVelocidad = costDistanciaHarversine/costTiempoDeViaje;
		this.cantidadTiempo = 1;
		this.cantidadDistancia = 1;
	}

	public double getCostDistanciaHarversine() {
		if(cantidadDistancia == 0)
			addCostDistanciaHarversine(Haversine.distance(idInterseccionOrigen.getLatitud(), idInterseccionOrigen.getLongitud(), idInterseccionDestino.getLatitud(), idInterseccionDestino.getLongitud()));
		return costDistanciaHarversine/cantidadDistancia;
	}

	public void addCostDistanciaHarversine(double costDistanciaHarversine) {
		this.costDistanciaHarversine += costDistanciaHarversine;
		cantidadDistancia++;
	}

	public double getCostTiempoDeViaje() {
		if(cantidadTiempo == 0)
			addCostTiempoDeViaje(idInterseccionOrigen.getMOVEMENT_ID() == idInterseccionDestino.getMOVEMENT_ID() ? 10 : 100);
		return costTiempoDeViaje/cantidadTiempo;
	}

	public void addCostTiempoDeViaje(double costTiempoDeViaje) {
		this.costTiempoDeViaje += costTiempoDeViaje;
		cantidadTiempo++;
	}

	public double getCostVelocidad() {
		if(cantidadTiempo == 0 || cantidadDistancia == 0)
			updateCostVelocidad();
		return costVelocidad;
	}
	
	public void updateCostVelocidad() {
		this.costVelocidad = getCostDistanciaHarversine()/getCostTiempoDeViaje();
	}

	public Interseccion getIdInterseccionOrigen() {
		return idInterseccionOrigen;
	}

	public void setIdInterseccionOrigen(Interseccion idInterseccionOrigen) {
		this.idInterseccionOrigen = idInterseccionOrigen;
	}

	public Interseccion getIdInterseccionDestino() {
		return idInterseccionDestino;
	}

	public void setIdInterseccionDestino(Interseccion idInterseccionDestino) {
		this.idInterseccionDestino = idInterseccionDestino;
	}

	@Override
	public String toString() {
		return "Costo [costDistanciaHarversine=" + costDistanciaHarversine + ", costTiempoDeViaje=" + costTiempoDeViaje
				+ ", costVelocidadArco=" + costVelocidad + "]";
	}

}
