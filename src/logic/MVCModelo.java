package logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Iterator;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.teamdev.jxmaps.ac;

//import controller.ManejadorArchivos;
import data_structures.*;
//import data_structures.LinkedList.NodeListaEnlazada;
import logic.*;
import logic.Maps;



/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{

	public final static String DATOS_MALLA_VERTICES = "./data/bogota_vertices.txt";

	public final static String DATOS_MALLA_ARCOS = "./data/bogota_arcos.txt";

	public final static String DATOS_VIAJES_SEMANA = "./data/bogota-cadastral-2018-1-WeeklyAggregate.csv";

	public final int CANTIDAD_VERTICES = 225896;
	public static final int CANTIDAD_ZONAS = 1147;


	/**
	 * Atributos del modelo del mundo
	 */
	
	private ManejadorArchivos manejadorArchivos;
	
	private Grafo<Integer, Interseccion, Costo> grafo;
	

	
	

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		manejadorArchivos = new ManejadorArchivos();
		grafo = new Grafo<Integer, Interseccion, Costo>(CANTIDAD_VERTICES);
		
//		mallavialgrafo = new GrafoNoDirigido<Integer, Interseccion>(225894);
//		grafoNodirigidoCargadoConJson = new GrafoNoDirigido(225894);

		//mapa = new Maps(grafoNodirigido);
//
//		tablaConViajesSemana = new HashTable<>();
//		
//		intersecciones = new HashTable<Integer, Interseccion>();

	}


	public void CargarArchivos()
	{
		manejadorArchivos.CargarVertices(grafo);
		System.out.println("Cantidad de nodos:\t"+grafo.getCantNodos());
		
		manejadorArchivos.CargarArcos(grafo);
		System.out.println("CAntidad de arcos:\t"+grafo.getCantArcos());
	}

	public void CargarTiempos()
	{
		System.out.println("inicio de cargue");
		
		manejadorArchivos.CargarCostos(grafo);
		System.out.println("Fin cargue");
	}
	

	public void CargarGrafoJSON()
	{
		manejadorArchivos.crearArchivoJSON(grafo);
	}
	
	public void GenerarGrafoJSON()
	{
		grafo = new Grafo<Integer, Interseccion, Costo>(CANTIDAD_VERTICES);
		manejadorArchivos.cargrArchivoJSON(grafo);
	}
	
	
	public void MostrarMapa()
	{
		MostrarMapaGeneral(grafo);
	}
	
	
	private void MostrarMapaGeneral(Grafo<Integer, Interseccion, Costo> grafo)
	{
		Maps maps = new Maps(grafo);
	      maps.initFrame("Calles");
	}
	
	private void print(String linea)
	{
		System.out.println(linea);
	}
	
	public int DarIDMasCercano(double latitud, double longitud)
	{
		Interseccion masCercana = null;
		double minDistancia = Integer.MAX_VALUE;
		
		for(Interseccion intersecion : grafo.getValues())
		{
			double distancia = Haversine.distance(latitud, latitud, intersecion.getLatitud(), intersecion.getLongitud());
			if(distancia < minDistancia)
			{
				minDistancia = distancia;
				masCercana = intersecion;
			}
		}
		
		if(masCercana == null)
			return -1;
		else
			return masCercana.getIDNodo();
	}
	
	public Interseccion darInterseccion(int id)
	{
		return grafo.getVertice(id);
	}
	
	public void darNMenorVelocidad(int n)
	{
		MinPQ<InterseccionVM> colaMenorVelocidad = new MinPQ<>(CANTIDAD_VERTICES, new Comparator<InterseccionVM>() {
			@Override
			public int compare(InterseccionVM o1, InterseccionVM o2) {
				return Double.compare(o1.getVelocidadMEdia(), o2.getVelocidadMEdia());
			}
		});
		
		for(Interseccion interseccion : grafo.getValues())
		{
			
		}
	}



}
